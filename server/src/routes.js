const AuthenticationController 			= require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy 	= require('./policies/AuthenticationControllerPolicy')
const TopArtistsController 				= require('./controllers/TopArtistController');

module.exports=(app)=>{
	app.post('/register', 
		AuthenticationControllerPolicy.register,
		AuthenticationController.register)

	app.post('/login',
    	AuthenticationController.login)

	/*API key	363460562ea9cf0a56930626b8155e47*/
	app.get('/artists', 
		TopArtistsController.TopList)
}
