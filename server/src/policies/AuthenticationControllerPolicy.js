const Joi = require('joi')

module.exports={
	register(req,res,next){
		const schema ={
			email: Joi.string().email(),
			password: Joi.string().regex(
				new RegExp('^[a-zA-Z0-9]{8,32}$')
			)
		}

		const {error, value} = Joi.validate(req.body, schema)
		if(error){
			switch (error.details[0].context.key){
				case 'email':
					res.status(400).send({
						error:'pon un pinche correo, no mames!'
					})
					break
				case 'password':
					res.status(400).send({
						error:`tu contraseña es una mamada!:
						<br>
						1.- usale de la A a la Z, pequeñas o grandotas y numeros del 0 al 9
						<br>
						2.- y tiene que tener al menos 8 pinches caracteres
						`
					})
					break
				default:
					res.status(400).send({
						error:'registro invalido calale de nuevo luego'
					})
			}
		}else{
			next()
		}
	}
}
